CREATE TABLE ticket_items (
    location_id VARCHAR(55) NOT NULL,
    ticket_id VARCHAR(55) NOT NULL,
    menu_item_id VARCHAR(55) NOT NULL,
    quantity INT NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(location_id, ticket_id, menu_item_id)
);

CREATE INDEX location_menu_item_idx ON ticket_items (location_id, menu_item_id);