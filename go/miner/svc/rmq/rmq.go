package rmq

import (
	"context"
	"encoding/json"

	"omnivore/miner/internal/model/ticket"
	"omnivore/pkg/cserr"
	"omnivore/pkg/cslog"
	"omnivore/pkg/event"
	"omnivore/pkg/rabbitmq"

	"github.com/pkg/errors"
	"github.com/streadway/amqp"
)

// Consumer is analogous to the Server type in the `rest` and `grpc`
// packages in the `api` directory.  It is meant to function as a
// receiving apparatus for the asynchronous message.
type Consumer struct {
	Logger   cslog.Logger
	AMQPConn *amqp.Connection

	TicketClient ticket.Client
	TicketQueue  ticket.Queue
}

// Run actually starts the Consumer
//
// WARNING: This is a blocking operation
func (c *Consumer) Run(oldCtx context.Context) error {
	ctx, cnFn := context.WithCancel(oldCtx)
	errorCollector := make(chan error) // TODO: buffer this channel when additional listeners are added

	go c.consumeLocationCreatedQueue(ctx, errorCollector)

	// block until error is received
	err := <-errorCollector
	cnFn() // cancel context to reclaim resources

	return errors.Wrap(err, "could not consume from queues")
}

func (c *Consumer) consumeLocationCreatedQueue(ctx context.Context, errChan chan<- error) {
	ch, err := c.AMQPConn.Channel()
	if err != nil {
		errChan <- cserr.NewErrInternalWithMsg(err, "could not create channel on amqp connection")
		return
	}
	defer c.Logger.Close(ctx, ch)

	msgs, err := ch.Consume(
		rabbitmq.LocationCreatedQueue, // queue
		"LocationCreated",             // consumer
		false,                         // autoAck
		false,                         // exclusive
		false,                         // noLocal
		false,                         // noWait
		nil,                           // args
	)
	if err != nil {
		errChan <- cserr.NewErrInternalWithMsg(err, "could not begin consuming messages on channel")
		return
	}

	var e event.LocationCreated
	for {
		select {
		case <-ctx.Done():
			return
		case msg := <-msgs:
			// failures at this stage can be treated as malformed data rather than catastrophic network errors,
			// handle accordingly
			if err := json.Unmarshal(msg.Body, &e); err != nil {
				c.Logger.Error(ctx, "failed to read message on amqp channel", "queue", rabbitmq.LocationCreatedQueue, "error", err.Error(), "body", string(msg.Body))
				continue
			}

			c.Logger.Info(ctx, "received message", "LocationCreated", e)
			go ticket.DiscoverTicketsByLocationID(ctx, c.Logger, e.ID, c.TicketClient, c.TicketQueue)

			c.Logger.Info(ctx, "acknowledging message")
			if err := msg.Ack(false); err != nil {
				c.Logger.Error(ctx, "failed to acknowledge message delivery", "error", err)
			}
		}
	}
}
