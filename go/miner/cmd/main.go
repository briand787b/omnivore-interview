package main

import (
	"context"
	"log"
	"os"

	"omnivore/miner/internal/model/location"
	"omnivore/miner/internal/omnivore"
	omnivorermq "omnivore/miner/internal/rabbitmq"
	"omnivore/miner/svc/rmq"
	"omnivore/pkg/cslog"
	"omnivore/pkg/rabbitmq"

	"github.com/google/uuid"
)

func main() {
	l := cslog.NewCSLogger(log.New(os.Stdout, "", 0), uuid.New())

	ctx := cslog.StoreSpanIDTraceID(context.Background(), "main", "main")

	// TODO: use env vars
	rmqConn, err := rabbitmq.NewConnection(ctx, l, "amqp://user:password@rmq:5672/")
	if err != nil {
		log.Println("could not connect: ", err)
		os.Exit(2)
	}
	defer rmqConn.Close()

	client := omnivore.NewClient(l, "", "")
	queue := omnivorermq.NewQueue(l, rmqConn)

	go location.DiscoverLocations(ctx, l, client, queue)

	consumer := rmq.Consumer{
		Logger:       l,
		AMQPConn:     rmqConn,
		TicketClient: client,
		TicketQueue:  queue,
	}

	log.Println("running...")

	if err := consumer.Run(ctx); err != nil {
		log.Println("FATAL: ", err)
		os.Exit(1)
	}
}
