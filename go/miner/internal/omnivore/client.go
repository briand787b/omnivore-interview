package omnivore

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"omnivore/miner/internal/model/location"
	"omnivore/miner/internal/model/ticket"
	"omnivore/pkg/cslog"

	"github.com/pkg/errors"
)

// Client implements both the ticket.Client and location.Client interfaces.
// It speaks HTTP to a RESTful API of Omnivore
type Client struct {
	Logger          cslog.Logger
	OmnivoreBaseURL string
	OmnivoreAPIKey  string
	*http.Client
}

func NewClient(l cslog.Logger, omnivoreBaseURL, omnivoreAPIKey string) *Client {
	if omnivoreBaseURL == "" {
		omnivoreBaseURL = "https://api.omnivore.io/1.0"
	}

	if omnivoreAPIKey == "" {
		// TODO: extract into env var
		omnivoreAPIKey = "8aa4572037ab4ebdb62fed0120c07ac3"
	}

	return &Client{
		Logger:          l,
		OmnivoreBaseURL: omnivoreBaseURL,
		OmnivoreAPIKey:  omnivoreAPIKey,
		Client: &http.Client{
			Timeout: 30 * time.Second,
		},
	}
}

func (c *Client) GetLocationIterator(ctx context.Context) <-chan location.Iteration {
	ch := make(chan location.Iteration)
	go c.pushLocations(ctx, ch)
	return ch
}

func (c *Client) GetTicketIteratorByLocationID(ctx context.Context, locationID string) <-chan ticket.Iteration {
	ch := make(chan ticket.Iteration)
	go c.pushTickets(ctx, locationID, ch)
	return ch
}

func (c *Client) pushLocations(ctx context.Context, ch chan<- location.Iteration) {
	defer close(ch)
	defer c.Logger.Info(ctx, "done pushing locations")
	req, err := http.NewRequest("GET", c.OmnivoreBaseURL+"/locations", nil)
	if err != nil {
		ch <- location.Iteration{
			Err: errors.Wrap(err, "could not form request to Omnivore location API"),
		}

		return
	}

	req.Header.Add("api-key", c.OmnivoreAPIKey)

	vals := req.URL.Query()
	vals.Set("limit", "1")

	for i := 0; true; i++ {
		c.Logger.Info(ctx, "iterating to next location", "offset", i)
		vals.Set("start", strconv.Itoa(i))
		req.URL.RawQuery = vals.Encode()

		resp, err := c.Do(req)
		if err != nil {
			ch <- location.Iteration{Err: errors.Wrap(err, "could not complete request")}
			break
		}

		defer resp.Body.Close()
		if resp.StatusCode > 299 {
			ch <- location.Iteration{Err: errors.Wrapf(err, "location miner encountered non-2XX response code '%d'", resp.StatusCode)}
			return
		}

		bs, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			ch <- location.Iteration{Err: errors.Wrap(err, "could not read body")}
			return
		}

		var loc LocationResponse
		if err := json.Unmarshal(bs, &loc); err != nil {
			ch <- location.Iteration{Err: errors.Wrap(err, "could not unmarshall body into LocationResponse")}
			return
		}

		if len(loc.Embedded.Locations) < 1 {
			c.Logger.Info(ctx, "done iterating locations", "reason", "all locations seen")
			return
		}

		ch <- location.Iteration{
			Location: location.Location{
				ID:   loc.Embedded.Locations[0].ID,
				Name: loc.Embedded.Locations[0].Name,
			},
		}
	}

	c.Logger.Info(ctx, "done pushing locations")
}

func (c *Client) pushTickets(ctx context.Context, locationID string, ch chan<- ticket.Iteration) {
	defer close(ch)
	defer c.Logger.Info(ctx, "done pushing tickets")
	url := fmt.Sprintf(c.OmnivoreBaseURL+"/locations/%s/tickets", locationID)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		ch <- ticket.Iteration{
			Err: errors.Wrap(err, "could not form request to Omnivore location API"),
		}

		return
	}

	req.Header.Add("api-key", c.OmnivoreAPIKey)

	vals := req.URL.Query()
	vals.Set("limit", "1")

	for i := 0; true; i++ {
		vals.Set("start", strconv.Itoa(i))
		req.URL.RawQuery = vals.Encode()

		resp, err := c.Do(req)
		if err != nil {
			ch <- ticket.Iteration{Err: errors.Wrap(err, "could not complete request")}
			return
		}

		defer resp.Body.Close()
		if resp.StatusCode > 299 {
			ch <- ticket.Iteration{Err: errors.Wrapf(err, "ticket miner encountered non-2XX response code '%d'", resp.StatusCode)}
			return
		}

		bs, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			ch <- ticket.Iteration{Err: errors.Wrap(err, "could not read body")}
			return
		}

		var tic TicketResponse
		if err := json.Unmarshal(bs, &tic); err != nil {
			ch <- ticket.Iteration{Err: errors.Wrap(err, "could not unmarshal body into TicketResponse")}
			return
		}

		if len(tic.Embedded.Tickets) > 1 {
			ch <- ticket.Iteration{Err: errors.New("response unexpectedly has multiple tickets")}
			break
		}

		if len(tic.Embedded.Tickets) < 1 {
			// done
			return
		}

		tickIteration := GetTicketIteration(locationID, &tic)
		ch <- tickIteration
	}
}

func GetTicketIteration(locationID string, tic *TicketResponse) ticket.Iteration {
	idMenuItemMapping := make(map[string]struct {
		MenuItemID string
		Quantity   int
	})

	for _, itm := range tic.Embedded.Tickets[0].Embedded.Items {
		foundItem, ok := idMenuItemMapping[itm.Embedded.MenuItem.ID]
		if !ok {
			idMenuItemMapping[itm.Embedded.MenuItem.ID] = struct {
				MenuItemID string
				Quantity   int
			}{
				MenuItemID: itm.Embedded.MenuItem.ID,
				Quantity:   itm.Quantity,
			}
			continue
		}

		idMenuItemMapping[itm.Embedded.MenuItem.ID] = struct {
			MenuItemID string
			Quantity   int
		}{
			MenuItemID: itm.Embedded.MenuItem.ID,
			Quantity:   foundItem.Quantity + itm.Quantity,
		}
	}

	tickIteration := ticket.Iteration{
		Ticket: ticket.Ticket{
			ID:         tic.Embedded.Tickets[0].ID,
			LocationID: locationID,
		},
	}
	for _, itm := range idMenuItemMapping {
		tickIteration.Items = append(tickIteration.Items, struct {
			MenuItemID string
			Quantity   int
		}{
			MenuItemID: itm.MenuItemID,
			Quantity:   itm.Quantity,
		})
	}

	return tickIteration
}
