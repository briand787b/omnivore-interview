package omnivore

type LocationResponse struct {
	Embedded struct {
		Locations []struct {
			Links struct {
				Auth struct {
					Href string `json:"href"`
					Type string `json:"type"`
				} `json:"auth"`
				ClockEntries struct {
					Href string `json:"href"`
					Type string `json:"type"`
				} `json:"clock_entries"`
				Configuration struct {
					Href string `json:"href"`
					Type string `json:"type"`
				} `json:"configuration"`
				Discounts struct {
					Href string `json:"href"`
					Type string `json:"type"`
				} `json:"discounts"`
				Employees struct {
					Href string `json:"href"`
					Type string `json:"type"`
				} `json:"employees"`
				Jobs struct {
					Href string `json:"href"`
					Type string `json:"type"`
				} `json:"jobs"`
				Menu struct {
					Href string `json:"href"`
					Type string `json:"type"`
				} `json:"menu"`
				OrderTypes struct {
					Href string `json:"href"`
					Type string `json:"type"`
				} `json:"order_types"`
				PriceCheck struct {
					Href string `json:"href"`
					Type string `json:"type"`
				} `json:"price_check"`
				Reports struct {
					Href string `json:"href"`
					Type string `json:"type"`
				} `json:"reports"`
				RevenueCenters struct {
					Href string `json:"href"`
					Type string `json:"type"`
				} `json:"revenue_centers"`
				Self struct {
					Href string `json:"href"`
					Type string `json:"type"`
				} `json:"self"`
				ServiceCharges struct {
					Href string `json:"href"`
					Type string `json:"type"`
				} `json:"service_charges"`
				Shifts struct {
					Href string `json:"href"`
					Type string `json:"type"`
				} `json:"shifts"`
				Tables struct {
					Href string `json:"href"`
					Type string `json:"type"`
				} `json:"tables"`
				TenderTypes struct {
					Href string `json:"href"`
					Type string `json:"type"`
				} `json:"tender_types"`
				Terminals struct {
					Href string `json:"href"`
					Type string `json:"type"`
				} `json:"terminals"`
				Tickets struct {
					Href string `json:"href"`
					Type string `json:"type"`
				} `json:"tickets"`
				Transfers struct {
					Href string `json:"href"`
					Type string `json:"type"`
				} `json:"transfers"`
				VoidTypes struct {
					Href string `json:"href"`
					Type string `json:"type"`
				} `json:"void_types"`
			} `json:"_links"`
			Address struct {
				City    interface{} `json:"city"`
				Country interface{} `json:"country"`
				State   interface{} `json:"state"`
				Street1 interface{} `json:"street1"`
				Street2 interface{} `json:"street2"`
				Zip     interface{} `json:"zip"`
			} `json:"address"`
			AgentVersion  string      `json:"agent_version"`
			ConceptName   string      `json:"concept_name"`
			Created       int         `json:"created"`
			CustomID      interface{} `json:"custom_id"`
			Development   bool        `json:"development"`
			DisplayName   string      `json:"display_name"`
			GooglePlaceID interface{} `json:"google_place_id"`
			Health        struct {
				Agent struct {
					AverageCPU    interface{} `json:"average_cpu"`
					AverageMemory interface{} `json:"average_memory"`
					Healthy       bool        `json:"healthy"`
					Processes     interface{} `json:"processes"`
				} `json:"agent"`
				Healthy bool `json:"healthy"`
				System  struct {
					AverageCPU    interface{} `json:"average_cpu"`
					AverageMemory interface{} `json:"average_memory"`
					Healthy       bool        `json:"healthy"`
				} `json:"system"`
				Tickets struct {
					ResponseTime interface{} `json:"response_time"`
					Status       string      `json:"status"`
				} `json:"tickets"`
			} `json:"health"`
			ID             string      `json:"id"`
			Latitude       interface{} `json:"latitude"`
			Longitude      interface{} `json:"longitude"`
			Modified       int         `json:"modified"`
			Name           string      `json:"name"`
			Owner          string      `json:"owner"`
			Phone          string      `json:"phone"`
			PosType        string      `json:"pos_type"`
			Status         string      `json:"status"`
			Timezone       string      `json:"timezone"`
			UpdaterVersion string      `json:"updater_version"`
			Website        interface{} `json:"website"`
		} `json:"locations"`
	} `json:"_embedded"`
	Links struct {
		Self struct {
			Href string `json:"href"`
			Type string `json:"type"`
		} `json:"self"`
	} `json:"_links"`
	Count int `json:"count"`
	Limit int `json:"limit"`
}
