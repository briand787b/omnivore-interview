package omnivore_test

import (
	"context"
	"testing"
	"time"

	"omnivore/miner/internal/model/location"
	"omnivore/miner/internal/model/ticket"
	"omnivore/miner/internal/omnivore"
	"omnivore/pkg/cslog/cslogtest"
)

var _ ticket.Client = &omnivore.Client{}
var _ location.Client = &omnivore.Client{}

func TestClientGetLocationIterator(t *testing.T) {
	t.Parallel()
	logger := cslogtest.NewMockLogger()
	ctx, cnFn := context.WithCancel(context.Background())
	c := omnivore.NewClient(logger, "", "")

	go func() {
		time.Sleep(3 * time.Second)
		cnFn() // stop iteration looping
	}()

	iter := c.GetLocationIterator(ctx)
	var locs []location.Iteration
	for loc := range iter {
		if loc.Err != nil {
			t.Fatal("LocationIteration has non-nil error: ", loc.Err.Error())
		}
		locs = append(locs, loc)
	}

	if len(locs) < 1 {
		t.Fatal("failed to retrieve any location iterations")
	}
}

func TestClientGetTicketIterator(t *testing.T) {
	t.Parallel()
	const locationID = "cXdAGXki"
	logger := cslogtest.NewMockLogger()
	ctx, cnFn := context.WithCancel(context.Background())
	c := omnivore.NewClient(logger, "", "")

	go func() {
		time.Sleep(3 * time.Second)
		cnFn() // stop iteration looping
	}()

	iter := c.GetTicketIteratorByLocationID(ctx, locationID)
	var tics []ticket.Iteration
	for tic := range iter {
		if tic.Err != nil {
			t.Fatal("TicketIteration has non-nil error: ", tic.Err.Error())
		}
		tics = append(tics, tic)
	}

	if len(tics) < 1 {
		t.Fatal("failed to retrieve any location iterations")
	}
}
