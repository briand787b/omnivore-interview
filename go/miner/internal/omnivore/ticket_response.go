package omnivore

type TicketResponse struct {
	Embedded struct {
		Tickets []struct {
			Embedded struct {
				Discounts []interface{} `json:"discounts"`
				Employee  struct {
					Links struct {
						ClockEntries struct {
							Href string `json:"href"`
							Type string `json:"type"`
						} `json:"clock_entries"`
						OpenTickets struct {
							Href string `json:"href"`
							Type string `json:"type"`
						} `json:"open_tickets"`
						PayRates struct {
							Href string `json:"href"`
							Type string `json:"type"`
						} `json:"pay_rates"`
						Self struct {
							Href string `json:"href"`
							Type string `json:"type"`
						} `json:"self"`
					} `json:"_links"`
					CheckName  string      `json:"check_name"`
					FirstName  string      `json:"first_name"`
					ID         string      `json:"id"`
					LastName   string      `json:"last_name"`
					Login      string      `json:"login"`
					MiddleName interface{} `json:"middle_name"`
					PosID      string      `json:"pos_id"`
					StartDate  string      `json:"start_date"`
				} `json:"employee"`
				Items []struct {
					Embedded struct {
						Discounts []interface{} `json:"discounts"`
						MenuItem  struct {
							Embedded struct {
								MenuCategories []struct {
									Links struct {
										MenuCategoryType struct {
											Href string `json:"href"`
											Type string `json:"type"`
										} `json:"menu_category_type"`
										Self struct {
											Href string `json:"href"`
											Type string `json:"type"`
										} `json:"self"`
									} `json:"_links"`
									ID    string `json:"id"`
									Level int    `json:"level"`
									Name  string `json:"name"`
									PosID string `json:"pos_id"`
								} `json:"menu_categories"`
								PriceLevels []struct {
									Links struct {
										Self struct {
											Href string `json:"href"`
											Type string `json:"type"`
										} `json:"self"`
									} `json:"_links"`
									Barcodes     []interface{} `json:"barcodes"`
									ID           string        `json:"id"`
									Name         interface{}   `json:"name"`
									PricePerUnit int           `json:"price_per_unit"`
								} `json:"price_levels"`
							} `json:"_embedded"`
							Links struct {
								MenuCategories struct {
									Href string `json:"href"`
									Type string `json:"type"`
								} `json:"menu_categories"`
								OptionSets struct {
									Href string `json:"href"`
									Type string `json:"type"`
								} `json:"option_sets"`
								PriceLevels struct {
									Href string `json:"href"`
									Type string `json:"type"`
								} `json:"price_levels"`
								Self struct {
									Href string `json:"href"`
									Type string `json:"type"`
								} `json:"self"`
							} `json:"_links"`
							Barcodes     []interface{} `json:"barcodes"`
							ID           string        `json:"id"`
							InStock      bool          `json:"in_stock"`
							Name         string        `json:"name"`
							Open         bool          `json:"open"`
							OpenName     bool          `json:"open_name"`
							PosID        string        `json:"pos_id"`
							PricePerUnit int           `json:"price_per_unit"`
						} `json:"menu_item"`
						Modifiers []interface{} `json:"modifiers"`
					} `json:"_embedded"`
					Links struct {
						Discounts struct {
							Href string `json:"href"`
							Type string `json:"type"`
						} `json:"discounts"`
						MenuItem struct {
							Href string `json:"href"`
							Type string `json:"type"`
						} `json:"menu_item"`
						Modifiers struct {
							Href string `json:"href"`
							Type string `json:"type"`
						} `json:"modifiers"`
						Self struct {
							Href string `json:"href"`
							Type string `json:"type"`
						} `json:"self"`
					} `json:"_links"`
					Comment     interface{} `json:"comment"`
					ID          string      `json:"id"`
					IncludedTax int         `json:"included_tax"`
					Name        string      `json:"name"`
					Price       int         `json:"price"`
					Quantity    int         `json:"quantity"`
					Seat        int         `json:"seat"`
					Sent        bool        `json:"sent"`
					SentAt      int         `json:"sent_at"`
					Split       int         `json:"split"`
				} `json:"items"`
				OrderType struct {
					Links struct {
						Self struct {
							Href string `json:"href"`
							Type string `json:"type"`
						} `json:"self"`
					} `json:"_links"`
					Available bool   `json:"available"`
					ID        string `json:"id"`
					Name      string `json:"name"`
					PosID     string `json:"pos_id"`
				} `json:"order_type"`
				Payments []struct {
					Embedded struct {
						TenderType struct {
							Links struct {
								Self struct {
									Href string `json:"href"`
									Type string `json:"type"`
								} `json:"self"`
							} `json:"_links"`
							AllowsTips bool   `json:"allows_tips"`
							ID         string `json:"id"`
							Name       string `json:"name"`
							PosID      string `json:"pos_id"`
						} `json:"tender_type"`
					} `json:"_embedded"`
					Links struct {
						Self struct {
							Href string `json:"href"`
							Type string `json:"type"`
						} `json:"self"`
						TenderType struct {
							Href string `json:"href"`
							Type string `json:"type"`
						} `json:"tender_type"`
					} `json:"_links"`
					Amount   int         `json:"amount"`
					Change   int         `json:"change"`
					Comment  interface{} `json:"comment"`
					FullName interface{} `json:"full_name"`
					ID       string      `json:"id"`
					Last4    interface{} `json:"last4"`
					Status   interface{} `json:"status"`
					Tip      int         `json:"tip"`
					Type     string      `json:"type"`
				} `json:"payments"`
				RevenueCenter struct {
					Links struct {
						OpenTickets struct {
							Href string `json:"href"`
							Type string `json:"type"`
						} `json:"open_tickets"`
						Self struct {
							Href string `json:"href"`
							Type string `json:"type"`
						} `json:"self"`
						Tables struct {
							Href string `json:"href"`
							Type string `json:"type"`
						} `json:"tables"`
					} `json:"_links"`
					Default bool   `json:"default"`
					ID      string `json:"id"`
					Name    string `json:"name"`
					PosID   string `json:"pos_id"`
				} `json:"revenue_center"`
				ServiceCharges []interface{} `json:"service_charges"`
				VoidedItems    []interface{} `json:"voided_items"`
			} `json:"_embedded"`
			Links struct {
				Discounts struct {
					Href string `json:"href"`
					Type string `json:"type"`
				} `json:"discounts"`
				Employee struct {
					Href string `json:"href"`
					Type string `json:"type"`
				} `json:"employee"`
				Items struct {
					Href string `json:"href"`
					Type string `json:"type"`
				} `json:"items"`
				OrderType struct {
					Href string `json:"href"`
					Type string `json:"type"`
				} `json:"order_type"`
				Payments struct {
					Href string `json:"href"`
					Type string `json:"type"`
				} `json:"payments"`
				RevenueCenter struct {
					Href string `json:"href"`
					Type string `json:"type"`
				} `json:"revenue_center"`
				Self struct {
					Href string `json:"href"`
					Type string `json:"type"`
				} `json:"self"`
				ServiceCharges struct {
					Href string `json:"href"`
					Type string `json:"type"`
				} `json:"service_charges"`
				VoidedItems struct {
					Href string `json:"href"`
					Type string `json:"type"`
				} `json:"voided_items"`
			} `json:"_links"`
			AutoSend    bool `json:"auto_send"`
			ClosedAt    int  `json:"closed_at"`
			Correlation struct {
				Sequence interface{} `json:"sequence"`
				Source   interface{} `json:"source"`
			} `json:"correlation"`
			FireDate     interface{} `json:"fire_date"`
			FireTime     interface{} `json:"fire_time"`
			GuestCount   int         `json:"guest_count"`
			ID           string      `json:"id"`
			Name         interface{} `json:"name"`
			Open         bool        `json:"open"`
			OpenedAt     int         `json:"opened_at"`
			PosID        interface{} `json:"pos_id"`
			ReadyDate    interface{} `json:"ready_date"`
			ReadyTime    interface{} `json:"ready_time"`
			TicketNumber int         `json:"ticket_number"`
			Totals       struct {
				Discounts      int `json:"discounts"`
				Due            int `json:"due"`
				ExclusiveTax   int `json:"exclusive_tax"`
				InclusiveTax   int `json:"inclusive_tax"`
				Items          int `json:"items"`
				OtherCharges   int `json:"other_charges"`
				Paid           int `json:"paid"`
				ServiceCharges int `json:"service_charges"`
				SubTotal       int `json:"sub_total"`
				Tax            int `json:"tax"`
				Tips           int `json:"tips"`
				Total          int `json:"total"`
			} `json:"totals"`
			Void bool `json:"void"`
		} `json:"tickets"`
	} `json:"_embedded"`
	Links struct {
		Next struct {
			Href string `json:"href"`
			Type string `json:"type"`
		} `json:"next"`
		Self struct {
			Href string `json:"href"`
			Type string `json:"type"`
		} `json:"self"`
	} `json:"_links"`
	Count int `json:"count"`
	Limit int `json:"limit"`
}
