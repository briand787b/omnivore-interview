package rabbitmq_test

import (
	"omnivore/miner/internal/model/location"
	"omnivore/miner/internal/model/ticket"
	"omnivore/miner/internal/rabbitmq"
)

var _ ticket.Queue = &rabbitmq.Queue{}
var _ location.Queue = &rabbitmq.Queue{}
