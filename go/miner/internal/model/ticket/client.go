package ticket

import "context"

type Iteration struct {
	Ticket
	Err error
}

type Client interface {
	GetTicketIteratorByLocationID(ctx context.Context, locationID string) <-chan Iteration
}
