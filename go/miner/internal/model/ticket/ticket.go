package ticket

import (
	"context"
	"omnivore/pkg/cslog"
	"omnivore/pkg/event"
)

type Ticket struct {
	ID         string
	LocationID string
	Items      []struct {
		MenuItemID string
		Quantity   int
	}
}

func DiscoverTicketsByLocationID(ctx context.Context, logger cslog.Logger, locationID string, c Client, q Queue) {
	iter := c.GetTicketIteratorByLocationID(ctx, locationID)
	for tic := range iter {
		if tic.Err != nil {
			logger.Error(ctx, "failed to iterate location",
				"error", tic.Err.Error(),
			)
			continue
		}

		td := event.TicketDiscovered{
			TicketID:   tic.ID,
			LocationID: tic.LocationID,
		}

		for _, itm := range tic.Items {
			td.Items = append(td.Items, struct {
				MenuItemID string "json:\"menu_item_id\""
				Quantity   int    "json:\"quantity\""
			}{
				MenuItemID: itm.MenuItemID,
				Quantity:   itm.Quantity,
			})
		}

		if err := q.EmitTicketDiscoveredEvent(ctx, td); err != nil {
			logger.Error(ctx, "failed to emit event discovered location",
				"error", err.Error(),
			)
		}
	}
}
