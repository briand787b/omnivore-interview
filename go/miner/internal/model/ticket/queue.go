package ticket

import (
	"context"

	"omnivore/pkg/event"
)

type Queue interface {
	EmitTicketDiscoveredEvent(ctx context.Context, td event.TicketDiscovered) error
}
