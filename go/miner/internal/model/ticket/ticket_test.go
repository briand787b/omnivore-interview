package ticket_test

import (
	"context"
	"testing"

	"omnivore/miner/internal/model/ticket"
	"omnivore/miner/internal/model/ticket/tickettest"
	"omnivore/pkg/cslog/cslogtest"

	"omnivore/pkg/event"
)

func TestDiscoverTicketsByTicketID(t *testing.T) {
	t.Parallel()
	const locationID = "loc_1"
	ctx := context.Background()
	logger := cslogtest.NewMockLogger()

	ticChan := make(chan ticket.Iteration)
	go func() {
		for _, tic := range []ticket.Iteration{
			{Ticket: ticket.Ticket{ID: "1", LocationID: locationID}},
			{Ticket: ticket.Ticket{ID: "2", LocationID: locationID}},
		} {
			ticChan <- tic
		}
		close(ticChan)
	}()
	client := tickettest.Client{}
	// testify resolves ticChan as `chan ticket.Iteration`, which panics b/c not `<-chan ticket.Iteration`
	func(ch <-chan ticket.Iteration) {
		client.On("GetTicketIteratorByLocationID", ctx, locationID).Return(ch)
	}(ticChan)

	queue := tickettest.Queue{}
	queue.On("EmitTicketDiscoveredEvent", ctx, event.TicketDiscovered{
		TicketID:   "1",
		LocationID: locationID,
	}).Return(nil)
	queue.On("EmitTicketDiscoveredEvent", ctx, event.TicketDiscovered{
		TicketID:   "2",
		LocationID: locationID,
	}).Return(nil)

	ticket.DiscoverTicketsByLocationID(ctx, logger, locationID, &client, &queue)

	queue.AssertNumberOfCalls(t, "EmitTicketDiscoveredEvent", 2)
}
