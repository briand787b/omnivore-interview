package location

import "context"

type Iteration struct {
	Location
	Err error
}

type Client interface {
	GetLocationIterator(ctx context.Context) <-chan Iteration
}
