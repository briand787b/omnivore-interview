package location

import (
	"context"

	"omnivore/pkg/event"
)

type Queue interface {
	EmitLocationDiscoveredEvent(ctx context.Context, e event.LocationDiscovered) error
}
