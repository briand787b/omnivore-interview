package location

import (
	"context"
	"omnivore/pkg/cslog"
	"omnivore/pkg/event"
)

type Location struct {
	ID   string
	Name string
}

// DiscoverLocations discovers locations.  WARNING: This is a blocking function
func DiscoverLocations(ctx context.Context, logger cslog.Logger, c Client, q Queue) {
	iter := c.GetLocationIterator(ctx)
	for loc := range iter {
		logger.Info(ctx, "location discovered", "location_id", loc.ID, "location_name", loc.Name, "error", loc.Err)
		if loc.Err != nil {
			logger.Error(ctx, "failed to iterate location",
				"error", loc.Err.Error(),
			)
			continue
		}

		if err := q.EmitLocationDiscoveredEvent(ctx, event.LocationDiscovered{
			ID: loc.ID,
		}); err != nil {
			logger.Error(ctx, "failed to emit event discovered location",
				"error", err.Error(),
			)
		}
	}

	logger.Info(ctx, "no longer discovering locations")
}
