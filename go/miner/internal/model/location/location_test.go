package location_test

import (
	"context"
	"testing"

	"omnivore/miner/internal/model/location"
	"omnivore/miner/internal/model/location/locationtest"
	"omnivore/pkg/cslog/cslogtest"
	"omnivore/pkg/event"
)

func TestDiscoverLocations(t *testing.T) {
	t.Parallel()
	ctx := context.Background()
	logger := cslogtest.NewMockLogger()

	locChan := make(chan location.Iteration)
	go func() {
		for _, loc := range []location.Iteration{
			{Location: location.Location{ID: "1"}},
			{Location: location.Location{ID: "2"}},
		} {
			locChan <- loc
		}
		close(locChan)
	}()
	client := locationtest.Client{}
	// testify resolves locChan as `chan location.Iteration`, which panics b/c not `<-chan location.Iteration`
	func(ch <-chan location.Iteration) {
		client.On("GetLocationIterator", ctx).Return(ch)
	}(locChan)

	queue := locationtest.Queue{}
	queue.On("EmitLocationDiscoveredEvent", ctx, event.LocationDiscovered{
		ID: "1",
	}).Return(nil)
	queue.On("EmitLocationDiscoveredEvent", ctx, event.LocationDiscovered{
		ID: "2",
	}).Return(nil)

	location.DiscoverLocations(ctx, logger, &client, &queue)

	queue.AssertNumberOfCalls(t, "EmitLocationDiscoveredEvent", 2)
}
