package postgres

import (
	"context"

	"omnivore/pkg/cslog"
)

func logQuery(ctx context.Context, l cslog.Logger, qry string, args ...interface{}) {
	l.Query(ctx, qry,
		"args", args,
	)
}
