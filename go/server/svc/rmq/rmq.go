package rmq

import (
	"context"
	"encoding/json"

	"omnivore/pkg/cserr"
	"omnivore/pkg/cslog"
	"omnivore/pkg/event"
	"omnivore/pkg/rabbitmq"
	"omnivore/server/internal/model/location"
	"omnivore/server/internal/model/ticketitem"

	"github.com/pkg/errors"
	"github.com/streadway/amqp"
)

// Consumer is analogous to the Server type in the `rest` and `grpc`
// packages in the `api` directory.  It is meant to function as a
// receiving apparatus for the asynchronous message.
type Consumer struct {
	Logger   cslog.Logger
	AMQPConn *amqp.Connection

	LocationQueue   location.Queue
	LocationStore   location.Store
	TicketItemStore ticketitem.Store
}

// Run actually starts the Consumer
//
// WARNING: This is a blocking operation
func (c *Consumer) Run(oldCtx context.Context) error {
	ctx, cnFn := context.WithCancel(oldCtx)
	errorCollector := make(chan error) // TODO: buffer this channel when additional listeners are added

	go c.consumeLocationDiscoveredQueue(ctx, errorCollector)
	go c.consumeTicketDiscoveredQueue(ctx, errorCollector)

	// block until error is received
	err := <-errorCollector
	cnFn() // cancel context to reclaim resources

	return errors.Wrap(err, "could not consume from queues")
}

func (c *Consumer) consumeTicketDiscoveredQueue(ctx context.Context, errChan chan<- error) {
	ch, err := c.AMQPConn.Channel()
	if err != nil {
		errChan <- cserr.NewErrInternalWithMsg(err, "could not create channel on amqp connection")
		return
	}
	defer c.Logger.Close(ctx, ch)

	msgs, err := ch.Consume(
		rabbitmq.TicketDiscoveredQueue, // queue
		"TicketDiscovered",             // consumer
		false,                          // autoAck
		false,                          // exclusive
		false,                          // noLocal
		false,                          // noWait
		nil,                            // args
	)
	if err != nil {
		errChan <- cserr.NewErrInternalWithMsg(err, "could not begin consuming messages on channel")
		return
	}

	var e event.TicketDiscovered
	for {
		select {
		case <-ctx.Done():
			return
		case msg := <-msgs:
			// failures at this stage can be treated as malformed data rather than catastrophic network errors,
			// handle accordingly
			if err := json.Unmarshal(msg.Body, &e); err != nil {
				c.Logger.Error(ctx, "failed to read message on amqp channel", "queue", rabbitmq.TicketDiscoveredQueue, "error", err.Error(), "body", string(msg.Body))
				continue
			}

			c.Logger.Info(ctx, "received message", "LocationDiscovered", e)
			go ticketitem.PersistTicketItems(ctx, c.Logger, e, c.TicketItemStore)

			c.Logger.Info(ctx, "acknowledging message")
			if err := msg.Ack(false); err != nil {
				c.Logger.Error(ctx, "failed to acknowledge message delivery", "error", err)
			}
		}
	}
}

func (c *Consumer) consumeLocationDiscoveredQueue(ctx context.Context, errChan chan<- error) {
	ch, err := c.AMQPConn.Channel()
	if err != nil {
		errChan <- cserr.NewErrInternalWithMsg(err, "could not create channel on amqp connection")
		return
	}
	defer c.Logger.Close(ctx, ch)

	msgs, err := ch.Consume(
		rabbitmq.LocationDiscoveredQueue, // queue
		"LocationDiscovered",             // consumer
		false,                            // autoAck
		false,                            // exclusive
		false,                            // noLocal
		false,                            // noWait
		nil,                              // args
	)
	if err != nil {
		errChan <- cserr.NewErrInternalWithMsg(err, "could not begin consuming messages on channel")
		return
	}

	var e event.LocationDiscovered
	for {
		select {
		case <-ctx.Done():
			return
		case msg := <-msgs:
			// failures at this stage can be treated as malformed data rather than catastrophic network errors,
			// handle accordingly
			if err := json.Unmarshal(msg.Body, &e); err != nil {
				c.Logger.Error(ctx, "failed to read message on amqp channel", "queue", rabbitmq.LocationDiscoveredQueue, "error", err.Error(), "body", string(msg.Body))
				continue
			}

			c.Logger.Info(ctx, "received message", "LocationDiscovered", e)
			go location.CreateLocation(ctx, c.Logger, e.ID, c.LocationStore, c.LocationQueue)

			c.Logger.Info(ctx, "acknowledging message")
			if err := msg.Ack(false); err != nil {
				c.Logger.Error(ctx, "failed to acknowledge message delivery", "error", err)
			}
		}
	}
}
