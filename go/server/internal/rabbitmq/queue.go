package rabbitmq

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"omnivore/pkg/cserr"
	"omnivore/pkg/cslog"
	"omnivore/pkg/event"
	"omnivore/pkg/rabbitmq"

	"github.com/streadway/amqp"
)

// Queue is an implementation of a object.Queue
type Queue struct {
	l        cslog.Logger
	amqpConn *amqp.Connection
}

// NewQueue returns a new Queue using the provided connection
func NewQueue(l cslog.Logger, conn *amqp.Connection) *Queue {
	return &Queue{
		l:        l,
		amqpConn: conn,
	}
}

func (q *Queue) EmitLocationCreatedEvent(ctx context.Context, e event.LocationCreated) error {
	if q.amqpConn.IsClosed() {
		return cserr.NewErrInternal(fmt.Errorf("amqp connection is closed"))
	}

	eventJSON, err := json.Marshal(e)
	if err != nil {
		return cserr.NewErrInternalWithMsg(err, "could not marshal event to JSON")
	}

	ch, err := q.amqpConn.Channel()
	if err != nil {
		return cserr.NewErrInternalWithMsg(err, "could not get channel from amqp connection")
	}

	defer ch.Close()

	if err := ch.Publish(
		rabbitmq.LocationCreatedExchange, // exchange
		"",                               // routingKey
		false,                            // mandatory
		false,                            // immediate
		amqp.Publishing{
			ContentType: "application/json",
			Timestamp:   time.Now(),
			Body:        eventJSON,
		}, // message
	); err != nil {
		return cserr.NewErrInternalWithMsg(err, "could not publish message")
	}

	return nil
}
