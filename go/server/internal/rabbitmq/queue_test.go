package rabbitmq_test

import (
	"omnivore/server/internal/model/location"
	"omnivore/server/internal/rabbitmq"
)

var _ location.Queue = &rabbitmq.Queue{}
