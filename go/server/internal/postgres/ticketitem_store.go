package postgres

import (
	"context"

	"omnivore/pkg/cslog"
	"omnivore/pkg/postgres"
	"omnivore/server/internal/model/ticketitem"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
)

type TicketItemDTO struct {
	LocationID string `db:"location_id"`
	TicketID   string `db:"ticket_id"`
	MenuItemID string `db:"menu_item_id"`
	Quantity   int    `db:"quantity"`
}

func NewTicketItemDTOFromTicketItem(t ticketitem.TicketItem) TicketItemDTO {
	return TicketItemDTO{
		LocationID: t.LocationID,
		TicketID:   t.TicketID,
		MenuItemID: t.MenuItemID,
		Quantity:   t.Quantity,
	}
}

// TicketItemStore satisfies the ticketitem.Store interface
type TicketItemStore struct {
	db postgres.ExtFull
	l  cslog.Logger
}

// NewTicketItemStore creates a new PG-backed TicketItem
func NewTicketItemStore(l cslog.Logger, db *postgres.ExtFull) *TicketItemStore {
	return &TicketItemStore{
		db: *db,
		l:  l,
	}
}

func (s *TicketItemStore) UpsertTicketItem(ctx context.Context, ti ticketitem.TicketItem) error {
	tDTO := NewTicketItemDTOFromTicketItem(ti)
	qry, args, err := sqlx.Named(`
		INSERT INTO ticket_items
		(
			location_id,
			ticket_id,
			menu_item_id,
			quantity
		)
		VALUES
		(
			:location_id,
			:ticket_id,
			:menu_item_id,
			:quantity
		)
		ON CONFLICT
		ON CONSTRAINT ticket_items_pkey 
		DO UPDATE SET 
			quantity = :quantity
		RETURNING 
			quantity;`,
		&tDTO,
	)

	if err != nil {
		return errors.Wrap(err, "could not bind TicketItem to query")
	}

	qry = sqlx.Rebind(sqlx.DOLLAR, qry)

	var quantity int
	if err := sqlx.GetContext(ctx, s.db, &quantity, qry, args...); err != nil {
		return errors.Wrap(err, "could not execute query")
	}

	s.l.Info(ctx, "ticket item saved",
		"quantity", quantity,
		"location_id", ti.LocationID,
		"ticket_id", ti.TicketID,
		"menu_item_id", ti.MenuItemID,
	)
	return nil
}
