package postgres

import (
	"context"

	"omnivore/pkg/cserr"
	"omnivore/pkg/cslog"
	"omnivore/pkg/postgres"
	"omnivore/server/internal/model/stats"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
)

type MostOrderedItemDTO struct {
	MenuItemID string `db:"menu_item_id"`
	Quantity   int    `db:"quantity"`
}

func (d *MostOrderedItemDTO) MostOrderedItem() *stats.MostOrderedItem {
	return &stats.MostOrderedItem{
		MenuItemID: d.MenuItemID,
		Quantity:   d.Quantity,
	}
}

// StatsStore satisfies the stats.Store interface
type StatsStore struct {
	db postgres.ExtFull
	l  cslog.Logger
}

// NewStatsStore creates a new PG-backed LicenseStore
func NewStatsStore(l cslog.Logger, db *postgres.ExtFull) *StatsStore {
	return &StatsStore{
		db: *db,
		l:  l,
	}
}

func (s *StatsStore) GetMostOrderedItemByLocationID(ctx context.Context, locationID string) (*stats.MostOrderedItem, error) {
	var mois []MostOrderedItemDTO
	if err := sqlx.SelectContext(ctx, s.db, &mois, `
		SELECT
			menu_item_id,
			SUM(quantity) AS quantity
		FROM
			ticket_items
		WHERE
			location_id = $1
		GROUP BY
			location_id, 
			menu_item_id
		ORDER BY
			quantity DESC
		LIMIT 1;`,
		locationID,
	); err != nil {
		return nil, errors.Wrap(err, "could not execute query")
	}

	if len(mois) != 1 {
		return nil, cserr.NewErrNotFound(errors.Errorf("non-singular record(s) returned for location ID %s", locationID))
	}

	moi := mois[0].MostOrderedItem()
	return moi, nil
}
