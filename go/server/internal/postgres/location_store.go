package postgres

import (
	"context"
	"omnivore/pkg/cslog"
	"omnivore/pkg/postgres"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
)

type LocationStore struct {
	db postgres.ExtFull
	l  cslog.Logger
}

// NewLocationStore creates a new PG-backed Location
func NewLocationStore(l cslog.Logger, db *postgres.ExtFull) *LocationStore {
	return &LocationStore{
		db: *db,
		l:  l,
	}
}

func (s *LocationStore) LocationIDExists(ctx context.Context, locationID string) (bool, error) {
	var finds []struct {
		LocationID string `db:"location_id"`
	}
	if err := sqlx.SelectContext(ctx, s.db, &finds, `
		SELECT
			location_id
		FROM
			ticket_items
		WHERE
			location_id = $1;`,
		locationID,
	); err != nil {
		return false, errors.Wrap(err, "could not execute query")
	}

	if len(finds) > 0 {
		return true, nil
	}

	return false, nil
}
