package ticketitem

import (
	"context"

	"omnivore/pkg/cslog"
	"omnivore/pkg/event"

	"github.com/pkg/errors"
)

type TicketItem struct {
	LocationID string
	TicketID   string
	MenuItemID string
	Quantity   int
}

type TicketItems []TicketItem

func PersistTicketItems(ctx context.Context, logger cslog.Logger, e event.TicketDiscovered, s Store) {
	logger.Info(ctx, "persisting ticket item", "ticket_id", e.TicketID, "location_id", e.LocationID)
	tis := NewTicketItems(e)
	if err := tis.Save(ctx, s); err != nil {
		logger.Error(ctx, "could not save ticket items", "error", err.Error())
	}
}

func NewTicketItems(e event.TicketDiscovered) TicketItems {
	tis := make([]TicketItem, len(e.Items))
	for i, itm := range e.Items {
		tis[i] = TicketItem{
			LocationID: e.LocationID,
			TicketID:   e.TicketID,
			MenuItemID: itm.MenuItemID,
			Quantity:   itm.Quantity,
		}
	}

	return TicketItems(tis)
}

func (t TicketItems) Save(ctx context.Context, s Store) error {
	if len(t) < 1 {
		return nil
	}

	for _, ti := range t {
		if err := s.UpsertTicketItem(ctx, ti); err != nil {
			return errors.Wrap(err, "could not upsert ticket item")
		}
	}

	return nil
}
