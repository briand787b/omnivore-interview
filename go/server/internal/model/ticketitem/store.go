package ticketitem

import "context"

type Store interface {
	UpsertTicketItem(ctx context.Context, ti TicketItem) error
}
