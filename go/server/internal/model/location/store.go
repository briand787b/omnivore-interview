package location

import "context"

type Store interface {
	LocationIDExists(ctx context.Context, locationID string) (bool, error)
}
