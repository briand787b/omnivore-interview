package location

import (
	"context"

	"omnivore/pkg/event"
)

type Queue interface {
	EmitLocationCreatedEvent(ctx context.Context, e event.LocationCreated) error
}
