package location

import (
	"context"

	"omnivore/pkg/cslog"
	"omnivore/pkg/event"
)

func CreateLocation(ctx context.Context, logger cslog.Logger, locationID string, s Store, q Queue) {
	if locationID == "" {
		logger.Error(ctx, "locationID is empty string")
		return
	}

	exists, err := s.LocationIDExists(ctx, locationID)
	if err != nil {
		logger.Error(ctx, "could not query store for locationID",
			"locationID", locationID,
			"error", err.Error(),
		)
		return
	}

	if exists {
		logger.Info(ctx, "location exists; doing nothing", "location_id", locationID)
		return
	}

	if err := q.EmitLocationCreatedEvent(ctx, event.LocationCreated{ID: locationID}); err != nil {
		logger.Error(ctx, "could not emit LocationCreated event",
			"error", err.Error(),
		)
		return
	}
}
