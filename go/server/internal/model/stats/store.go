package stats

import "context"

type Store interface {
	GetMostOrderedItemByLocationID(ctx context.Context, locationID string) (*MostOrderedItem, error)
}
