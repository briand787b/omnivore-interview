package stats

import (
	"context"
	"omnivore/pkg/cserr"

	"github.com/pkg/errors"
)

// TODO: add item_name to this struct
type MostOrderedItem struct {
	MenuItemID string
	Quantity   int
}

func GetMostOrderedItemByLocation(ctx context.Context, locationID string, s Store) (*MostOrderedItem, error) {
	if locationID == "" {
		return nil, cserr.NewErrInvalid("locationID is empty string")
	}

	moi, err := s.GetMostOrderedItemByLocationID(ctx, locationID)
	if err != nil {
		return nil, errors.Wrap(err, "could not query most ordered item by location")
	}

	return moi, nil
}
