package main

import (
	"context"
	"log"
	"os"

	"omnivore/pkg/cslog"
	"omnivore/pkg/postgres"
	"omnivore/pkg/rabbitmq"
	ompostgres "omnivore/server/internal/postgres"
	omnivorermq "omnivore/server/internal/rabbitmq"
	"omnivore/server/svc/rest"
	"omnivore/server/svc/rmq"

	"github.com/google/uuid"
)

func main() {
	l := cslog.NewCSLogger(log.New(os.Stdout, "", 0), uuid.New())

	ctx := cslog.StoreSpanIDTraceID(context.Background(), "main", "main")

	// TODO: use env vars
	rmqConn, err := rabbitmq.NewConnection(ctx, l, "amqp://user:password@rmq:5672/")
	if err != nil {
		log.Println("could not connect: ", err)
		os.Exit(2)
	}
	defer rmqConn.Close()

	extFull := postgres.NewExtFull(l, &postgres.ExtFullParams{
		Host:     os.Getenv("PG_HOST"),
		DB:       os.Getenv("PG_DB"),
		User:     os.Getenv("PG_USER"),
		Password: os.Getenv("PG_PASS"),
		Port:     os.Getenv("PG_PORT"),
	})

	ss := ompostgres.NewStatsStore(l, extFull)

	srv, err := rest.NewServer(&rest.ServerParams{
		Port:       os.Getenv("PORT"),
		Logger:     l,
		StatsStore: ss,
	})
	if err != nil {
		log.Println("could not start server: ", err)
		os.Exit(3)
	}
	go srv.Run()

	queue := omnivorermq.NewQueue(l, rmqConn)
	ts := ompostgres.NewTicketItemStore(l, extFull)
	ls := ompostgres.NewLocationStore(l, extFull)
	consumer := rmq.Consumer{
		Logger:          l,
		AMQPConn:        rmqConn,
		LocationQueue:   queue,
		TicketItemStore: ts,
		LocationStore:   ls,
	}

	// this will block and cause everything below it to not work, but
	// don't worry about it until later
	if err := consumer.Run(ctx); err != nil {
		log.Println("FATAL: ", err)
		os.Exit(1)
	}
}
