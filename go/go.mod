module omnivore

go 1.16

require (
	github.com/go-chi/chi v1.5.4
	github.com/go-chi/render v1.0.1
	github.com/google/uuid v1.2.0
	github.com/jmoiron/sqlx v1.3.4
	github.com/lib/pq v1.10.2
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/onsi/gomega v1.14.0 // indirect
	github.com/pkg/errors v0.9.1
	github.com/sergi/go-diff v1.2.0 // indirect
	github.com/streadway/amqp v1.0.0
	github.com/stretchr/testify v1.5.1
	github.com/yudai/gojsondiff v1.0.0
	github.com/yudai/golcs v0.0.0-20170316035057-ecda9a501e82 // indirect
	github.com/yudai/pp v2.0.1+incompatible // indirect
)
